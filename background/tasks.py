from db import update_data, get_all_data, get_table
import datetime


def reporting_loc():
    print("done")
    # users = get_all_data()
    # for user in users:
    #     last_reported = datetime.datetime.strptime(
    #         str(user['dateLastLogged']), '%Y-%m-%d %H:%M:%S')
    #     now = datetime.datetime.now()
    #     if now.hour - last_reported.hour >= 1 or now.day - last_reported.day >= 1:
    #         points = user['points'] - 20
    #         update_data(points, 'points', user['id'], 'userData')


def update_points():
    users = get_table('userData')
    for user in users:
        if user['doesHavePotato'] == 1:
            points = user['points'] - 5
            update_data(points, 'points', user['id'], 'userData')
