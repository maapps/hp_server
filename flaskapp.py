import os
from datetime import datetime
from flask import Flask, request, flash, url_for, redirect, \
    render_template, abort, send_from_directory, jsonify
import db
import json
from geopy.distance import distance
from random import randint
from flask_mail import Mail, Message

app = Flask(__name__)
mail = Mail(app)
app.config.from_pyfile('flaskapp.cfg')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<path:resource>')
def serveStaticResource(resource):
    return send_from_directory('static/', resource)

'''
@app.route('/verify_user')
@authorized
def root(self, userid):
    return "Hello {}".format(userid)
'''


@app.route("/test")
def test():
    print 'TEST @/test'
    return "<h1>This is a test</h1>\
    <h1>Fridge</h1>\
    <h1>Flask</h1>\
    <h1>Email</h1>\
    <h1>jacob_aronoff16@milton.edu</h1>"


@app.route("/getData")
def unionData():
    toPrint = db.get_all_data()
    if toPrint is not None:
        return json.dumps(toPrint)
    else:
        return json.dumps({"status": "success"})


@app.route("/new_user", methods=['POST', 'GET'])
def new_user():
    print 'TEST'
    post = request.json
    name = post['name']
    password = post['password']
    email = post['email']
    print 'RECEIVED MESSAGE'
    print str(post)
    key_to_verify = db.new_user(name, password, email, datetime.now())
    msg = Message(
        'Hello, thanks for creating an account with HotPotato',
        sender='miltonprogrammer@gmail.com',
        recipients=[email])
    msg.body = 'Hello, thanks for registering to be a user. Click this link to register www.hotpotatoapp.xyz/authenticate_user/{0}'.format(
        str(key_to_verify))
    print str(msg.body)
    mail.send(msg)
    if key_to_verify is not None:
        return json.dumps({"status": "Successssss"})
    else:
        return json.dumps({"status": "success"})


@app.route("/authenticate_user/<key>")
def authenticate(key=None):
    users = db.get_table('unverifiedUsers')
    username = None
    for user in users:
        if user['verifyKey'] == key:
            username = user['username']
            db.verify_user(user)
    return render_template('authenticate.html', verifyKey=key, username=username)


@app.route("/login", methods=['POST'])
def login_user():
    post = request.json
    toPrint = db.login(post['email'], post["password"])
    return json.dumps({"status": toPrint})


@app.route("/leaderboard", methods=["GET"])
def get_leaders():
    toPrint = db.get_leaderboard()
    return (json.dumps({"status": "Success",
                        "response": toPrint}))


@app.route("/getData/<table_name>")
def get_data(table_name):
    toPrint = db.get_table(table_name)
    if toPrint is not None:
        return json.dumps({"status": toPrint})
    else:
        return json.dumps({"status": "success"})


@app.route("/getUserID", methods=['POST', 'GET'])
def getUserID():
    post = request.json
    email = post['email']
    toPrint = db.get_user_id(email)
    if toPrint is not None:
        return json.dumps({"status": toPrint})
    else:
        return json.dumps({"status": "success"})


@app.route("/sendData", methods=['POST', 'GET'])
def sendData():
    post = request.json
    lat = post['lat']
    lon = post['long']
    email = post['email']
    isOn = post['locationOn']
    date = datetime.now()
    toPrint = db.send_location(email, lat, lon, isOn, date)
    if toPrint is not None:
        return json.dumps({"status": toPrint})
    else:
        return json.dumps({"status": "success"})


@app.route('/near', methods=['POST', 'GET'])
def near():
    post = request.json
    user_loc = (post['lat'], post['lng'])
    uid = post['userID']
    near_list = []
    for person in db.get_table('locationData'):
        location = (person['lat'], person['lng'])
        if distance(user_loc, location).miles < 1:
            if uid != person['userID']:
                near_list.append(person['userID'])
    return near_list


@app.route('/givePotato', methods=['POST', 'GET'])
def givePotato():
    post = request.json
    email = post['email']
    db.update_data_email('0', "doesHavePotato", email, "userData")
    users = db.get_table('userData')
    while(True):
        victim = users[randint(0, len(users))]
        if victim['email'] == email:
            continue
        db.update_data_email('1', 'doesHavePotato',
                             victim['email'], 'userData')
        return json.dumps({'status': 'success', 'victim': victim['email']})


@app.route('/doihavePotato', methods=['POST', 'GET'])
def doihavePotato():
    post = request.json
    print(post)
    print(request)
    email = post['email']
    toReturn = db.get_potato(email)
    return json.dumps(toReturn)


@app.route('/uniqueemail', methods=['POST', 'GET'])
def uniqueEmail():
    email = request.json['email']
    users = db.get_table('userData')
    for user in users:
        if user['email'] == email:
            return json.dumps({'isUnique': '0'})
    return json.dumps({'isUnique': '1'})


@app.route('/getPoints', methods=['POST', 'GET'])
def getPoints():
    post = request.json
    email = post['email']
    toReturn = db.get_points(email)
    print str(toReturn)
    return json.dumps(toReturn)


if __name__ == '__main__':
    app.run()
