import MySQLdb
import settings
import string
import random
import MySQLdb.cursors as cursors
from datetime import datetime


db_config = settings.SQL_CONNECTION


def get_connection():
    return MySQLdb.connect(user=db_config['User'],
                           passwd=db_config['Password'],
                           host=db_config['SERVER'],
                           db=db_config['DATABASE'],
                           port=db_config['PORT'],
                           cursorclass=cursors.DictCursor)


def get_table(table_name):
    connection = get_connection()
    cursor = connection.cursor()
    cursor.execute('SELECT * from %s' % table_name)
    toReturn = []
    for x in xrange(0, cursor.rowcount):
        toReturn.append(fetch_Assoc_Array(cursor))
    return toReturn
    cursor.close()
    connection.close()


def get_potato(email):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = ("SELECT doesHavePotato FROM userData WHERE email = '{0}'").format(
            email)
        cursor.execute(sql)
        toReturn = cursor.fetchone()
        return toReturn
    finally:
        cursor.close()
        connection.close()


def send_location(email, lat, lon, isOn, date):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = """
        UPDATE locationData SET lat=%s,
            lng=%s, zcoord=%s,dateReceived=%s,
            isOn=%s
            WHERE userID = %s
            """
        cursor.execute(sql, [lat, lon, 12, date, 1, find_user(
            get_table('userData'), email)['id']])
        connection.commit()
        return get_potato(email)
    finally:
        cursor.close()
        connection.close()


def login(email, password):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = """
            SELECT 'name' FROM userData
            WHERE email='{0}' AND password='{1}'
        """.format(email, password)
        cursor.execute(sql)
        response = cursor.fetchone()
        if(response is not None):
            return 'Success'
        else:
            return 'Failure'
    finally:
        cursor.close()
        connection.close()


def verify_user(user):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        print(user)
        sql = """
            INSERT INTO userData (name, email, doesHavePotato,
            points, password)
            VALUES ('{0}','{1}','{2}','{3}','{4}')
        """.format(user['username'], user['email'], 0, 0, user['password'])
        cursor.execute(sql)
        connection.commit()
        new_id = find_user(get_table('userData'), user['email'])['id']
        sql = """
        INSERT INTO locationData (lat,
            lng, zcoord, userID, dateReceived,
            isOn)
            VALUES (%s, %s, %s, %s, %s, %s)
            """
        cursor.execute(sql, [0, 0, 12,  new_id, datetime.now(), 0])
        connection.commit()
        sql = """
            DELETE from unverifiedUsers where id={0}
        """.format(user['id'])
        cursor.execute(sql)
        connection.commit()
    finally:
        cursor.close()
        connection.close()


def find_user(users, email):
    for user in users:
        print(user)
        if email in user['email']:
            return user
    return None


def new_user(name, password, email, date):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        unverifiedUsers = get_table('unverifiedUsers')
        users = get_table('userData')
        key_to_verify = key_generator(size=20)
        if (email in unverifiedUsers) or (email in users):
            return "User already exists"
        sql = """
            INSERT INTO unverifiedUsers (username,
            password, email, verifyKey)
            VALUES ('{0}','{1}','{2}','{3}')
            """.format(name, password, email, key_to_verify)
        print(sql)
        cursor.execute(sql)
        connection.commit()
        return key_to_verify
    finally:
        cursor.close()
        connection.close()


def auth_user(email, password):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = """
        SELECT * FROM userData WHERE email=%s AND password=%s
        """
        toReturn = cursor.execute(sql, [email, password])
        connection.commit()
        return toReturn
    finally:
        cursor.close()
        connection.close()


def get_user_id(email):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        cursor.execute(
            'SELECT id from %s WHERE email = %s' %
            ('userData', email))
        toReturn = cursor.fetchone()
        return toReturn
    finally:
        cursor.close()
        connection.close()


def get_leaderboard():
    connection = get_connection()
    cursor = connection.cursor()
    try:
        toReturn = []
        sql = """
            SELECT  `name` ,  `points`,  `email` FROM  `userData`
        """
        cursor.execute(sql)
        for row in cursor.fetchall():
            toReturn.append(row)
        return toReturn
    finally:
        cursor.close()
        connection.close()


def get_points(email):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = "SELECT points from userData WHERE email = '{0}'".format(
            email)
        cursor.execute(sql)
        toReturn = cursor.fetchone()
        return toReturn
    finally:
        cursor.close()
        connection.close()


def get_all_data():
    userData = get_table("userData")
    locationData = get_table("locationData")
    for dictionary in userData:
        dictionary['locations'] = []
        for location in locationData:
            if location['userID'] == dictionary['id']:
                dictionary['locations'].append(location)
    return userData


def fetch_Assoc_Array(cursor):
    data = cursor.fetchone()
    if data is None:
        return None

    dictionaryToReturn = {}
    for (key, value) in data.iteritems():
        dictionaryToReturn[key] = value

    return dictionaryToReturn


def update_loc(email, lat, lon):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = 'UPDATE locationData SET LATITUDE = %s, LONGITUDE = %s'
        'WHERE email = %s' % (
            lat, lon, email)
        cursor.execute(sql)
        connection.commit()
    finally:
        cursor.close()
        connection.close()


def update_data_email(data, col, email, table_name):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = "UPDATE `{0}` SET `{1}` = {2} WHERE `email` = '{3}'".format(
            table_name, col, data, email)
        cursor.execute(sql)
        connection.commit()
    finally:
        cursor.close()
        connection.close()


def key_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def update_data(data, col, id, table_name):
    connection = get_connection()
    cursor = connection.cursor()
    try:
        sql = "UPDATE {0} SET `{1}` = {2} WHERE id = {3}".format(
            table_name, col, data, id)
        cursor.execute(sql)
        connection.commit()
    finally:
        cursor.close()
        connection.close()
