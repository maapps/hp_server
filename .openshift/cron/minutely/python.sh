#!/usr/bin/env bash
today=`date +%Y-%m-%d.%H:%M:%S`
echo 'Python Tasks Started at: ' + $today
python ${OPENSHIFT_REPO_DIR}/background/run_tasks.py
echo '----Finished Running Tasks----'
